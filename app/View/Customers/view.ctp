<?PHP
$totalcurrentvalue=0;
$totaloriginalvalue=0;
$totalacquiredvalue=0;
$totalrecentvalue=0;
$totalpfcurrentvalue=0;
$totalpforiginalvalue=0; ?>


<div class="customers view">
<h2><?php echo __('Customer'); ?></h2>

              
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Streetaddress'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['streetaddress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Primaryemail'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['primaryemail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Homephone'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['homephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cellphone'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['cellphone']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div>
<div class="related">
	<h3><?php echo __('Stocks'); ?></h3>
	<?php if (!empty($customer['Stock'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		
		<th><?php echo __('Stsymbol'); ?></th>
		
		<th><?php echo __('Noshares'); ?></th>
		<th><?php echo __('Purchaseprice'); ?></th>
		<th><?php echo __('Datepurchased'); ?></th>
		<th><?php echo __('Original Value'); ?></th>
		<th><?php echo __('Current Price'); ?></th>
		<th><?php echo __('Current value'); ?></th>
		
		
	</tr>
	<?php 
	$currentprice=50;
	foreach ($customer['Stock'] as $stock):
require_once('nusoap.php'); 

$c = new nusoap_client('http://loki.ist.unomaha.edu/~groyce/ws/stockquoteservice.php');

$Currentprice = $c->call('getStockQuote',array('symbol' => $stock['stsymbol']));
	?>
		<tr>
			
			<td><?php echo $stock['stsymbol']; ?></td>
			
			<td><?php echo $stock['noshares']; ?></td>
			<td><?php echo $stock['purchaseprice']; ?></td>
			<td><?php echo $stock['datepurchased']; ?></td>
			<td>$ <?php echo ($stock['purchaseprice'] * $stock['noshares'] )?></td>
		 <?php $totaloriginalvalue = $totaloriginalvalue +($stock['purchaseprice'] * $stock['noshares'] )?>
		<td>$ <?php echo $currentprice?></td>
		<td>$ <?php echo $currentprice * $stock['noshares'] ?></td>
		 <?php $totalcurrentvalue = $totalcurrentvalue +($currentprice * $stock['noshares'] )?>
			
		</tr>
	<?php endforeach; ?>
	<tr>
	<td><b>Total Stock Value</b></td>
	<td></td>
	<td></td>
	<td></td>
	
	<td><b>$ <?php echo $totaloriginalvalue; ?><b></td>
	<td></td>
	<td><b>$ <?php echo $totalcurrentvalue; ?><b></td>
	</tr>
	</table>
<?php endif; ?>

	
</div>

<div class="related">
	<h3><?php echo __('Investments'); ?></h3>
	<?php if (!empty($customer['Investment'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		
		<th><?php echo __('Category'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Acquired value'); ?></th>
		<th><?php echo __('Acquired date'); ?></th>
		<th><?php echo __('Recent value'); ?></th>
		<th><?php echo __('Recent date'); ?></th>
		
	</tr>
	<?php foreach ($customer['Investment'] as $investment): ?>
		<tr>
			
			<td><?php echo $investment['category']; ?></td>
			<td><?php echo $investment['description']; ?></td>
			<td><?php echo $investment['acquiredvalue']; ?></td>
			<?php $totalacquiredvalue = $totalacquiredvalue +($investment['acquiredvalue'])?>
			<td><?php echo $investment['acquireddate']; ?></td>
			<td><?php echo $investment['recentvalue']; ?></td>
			<?php $totalrecentvalue = $totalrecentvalue +($investment['recentvalue'])?>
			<td><?php echo $investment['recentdate']; ?></td>
			
		</tr>
	<?php endforeach; ?>
	<tr>
	
	<td><b>Total Investent Value</b></td>
	<td></td>
	
	<td><b>$ <?php echo $totalacquiredvalue; ?><b></td>
	<td></td>
	<td><b>$ <?php echo $totalrecentvalue; ?><b></td>
	<td></td>
	
	</tr>
	</table>
<?php endif; ?>

	
</div>

<div class="related">
	<h3><?php echo __('Total Portfolio Value'); ?></h3>
	<?php if (!empty($customer['Stock'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		
		<th><?php echo __('Category'); ?></th>
		
		
		<th><?php echo __('Original Value'); ?></th>
		
		<th><?php echo __('Current value'); ?></th>
		
		
	</tr>
	
		<tr>
			<td><?php echo __('Stocks'); ?></td>
			
			<td><b>$ <?php echo $totaloriginalvalue; ?><b></td>
		<td><b>$ <?php echo $totalcurrentvalue; ?><b></td>
		
			
		</tr>
	<tr>
			<td><?php echo __('Investments'); ?></td>
			
			<td><b>$ <?php echo $totalacquiredvalue; ?><b></td>
		<td><b>$ <?php echo $totalrecentvalue; ?><b></td>
		
			
		</tr>
		<tr>
			<td><?php echo __('Total Portfolio Value'); ?></td>
			 <?php $totalpforiginalvalue = $totaloriginalvalue + $totalacquiredvalue?>
			<td>$ <?php echo $totalpforiginalvalue; ?></td>
			 <?php $totalpfcurrentvalue = $totalcurrentvalue + $totalrecentvalue?>
		<td>$ <?php echo $totalpfcurrentvalue; ?></td>
		
			
		</tr>
	</table>
<?php endif; ?>

	
</div>
